# Thesis

This repo contains the pre-executed notebooks that were used to answer each research question in my thesis. It is divided into 3 folders;

*) RQ1: has two notebooks: 
                        -Traversing CelebA: it shows the traversals of every learned latent code.
                        -CelebaA Losses: It contains the loss plots for all the models trained on CelebA, as well as the discovered generative factors and the combined latent codes that were used to create the new samples.

*) RQ2: has two notebooks:
                        - VAE metrics that has all the disentanglement metrics for beta-VAE, betaTC-VAE, FactorVAE and simple VAE.
                        - RQ2 which answers the linearity question raised in the second research question.

*) RQ3: has two notebooks as well:
                                -The first one has the interpolation and extrapolation results on dSprites as well as smoothness evaluation.
                                - The second notebook tackles interpolation, extrapolation and smoothness on MNIST. 

